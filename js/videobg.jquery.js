/* ARRAY NAVIGATION */
Array.prototype.next = function() {
    if ( (this.current + 1) in this ){
	return this[++this.current];
    }else{
	return this[this.current = 0];
    }
};
Array.prototype.prev = function() {
    if ( (this.current - 1) in this ){
	return this[--this.current];
    }else{
	return this[this.current = (this.length - 1)];
    }
};
Array.prototype.curr = function(current){
    if ( typeof(current) == 'number' && (current in this) ){
	this.current = current;
    }
    return this[this.current];
}
Array.prototype.current = 0;
/* /ARRAY NAVIGATION */


;(function($) {
    $.detectVideoType = function( ){
	var type = null;
	    video = document.createElement('video');
	if ( video.canPlayType('video/x-m4v') ) type = 'm4v';
	else if ( video.canPlayType('video/mp4') ) type = 'mp4';
	else if ( video.canPlayType('video/ogg') ) type = 'ogg';
	else if ( video.canPlayType('video/webm') ) type = 'webm';
	return type;
    }
    $.fn.videobg = function( options ){
	var defaults = {
	    videosets : [],
	    init : {
		videoset : 1,
		video : 1
	    },
	    loop : false,
	    loopLast : false,
	    controls : null,
	    onVideoChange : function(label, videoset, video){},
	    onChangePlay : function(label, videoset, video){},
	    onPauseChange : function(pause){},
	    onMuteChange : function(mute){},
	    onPlaybackChange : function(currentTime, duration){},
	    onRepeatPlay : function(){},
	    onRepeat : function(){},
	    onInit : function(){}
	};
	var options = $.extend(defaults, options);
	
	return this.each(function(){
	    var	$this = this,
		$Changed = true,
		$Options = options,
		$Storage = [],
		$StorageMap = [],
		$VideoType = $.detectVideoType(),
		$Container = $(this)
		$Init = false;
	    var rnd = Math.round(Math.random() * 1000000),
		current = 0;
	    for ( var keySet in $Options.videosets  ){
		$StorageMap[keySet] = [];
		for ( var keyVideo in $Options.videosets[keySet] ){
		    if ( $Options.videosets[keySet].hasOwnProperty(keyVideo) ){
			rnd ++;
			var $Item = {},
			    ID = 'video-' + rnd;
			$('<video preload="auto" webkit-playinline></video>').attr('id', ID).appendTo($Container);
			var $Video = document.getElementById(ID);
			$Video.autoplay = false;
			if ( $Options.videosets[keySet][keyVideo].poster ) $Video.poster = $Options.videosets[keySet][keyVideo].poster;
			if ( $Options.videosets[keySet][keyVideo][$VideoType] ) $Video.src = $Options.videosets[keySet][keyVideo][$VideoType];
			$Video.load();
			$Video.addEventListener('loadstart', function(){
			    if ( this == $Storage.curr().video ){
				$Preloader.fadeIn();
			    }
			});
			$Video.addEventListener('canplay', function(){
			});
			$Video.addEventListener('canplaythrough', function(){
			    if ( this === $Storage[0].video && !$Init ){
				$Init = true;
				$Preloader.fadeOut();
				$Play.fadeIn();
				$Options.onInit.call($this);
			    }
			});
			$Video.addEventListener('playing', function(){});
			$Video.addEventListener('play', function(){});
			$Video.addEventListener('timeupdate', function(){
			    var video = this;
			    $Options.onPlaybackChange.call($this, video.currentTime, video.duration);
			});
			$Video.addEventListener('pause', function(){
			});
			$Video.addEventListener('ended', function(){
			    if ( $Storage.curr().index == ($Storage.length - 1) ){
				if ( $Options.loop ) PlayNextVideo(true);
				else if ( $Options.loopLast ) PlayVideo();
			    }else PlayNextVideo(true);
			});
			$StorageMap[keySet][keyVideo] = current;
			$Storage.push({
			    index : current,
			    label : $Options.videosets[keySet][keyVideo].label,
			    keySet : parseInt(keySet) + 1,
			    keyVideo : parseInt(keyVideo) + 1,
			    video : $Video
			});
			if ( keySet == ($Options.init.videoset - 1) && keyVideo == ($Options.init.video - 1) ){
			    $Storage.curr(current);
			    $Options.onVideoChange.call(this, $Storage.curr().label, $Storage.curr().keySet, $Storage.curr().keyVideo);
			}else{
			    $($Video).hide();
			}
			current ++;
		    }
		}
	    }
	    
	    $(this).on('set', function(event){
		var videoCurr = $Storage.curr();
		if ( $Storage.curr().index != $StorageMap[event.videoset-1][event.video - 1]){
		    PlayVideo($StorageMap[event.videoset-1][event.video - 1]);
		    $Default.show();
		    $Additional.hide();
		}
	    });
	    
	    var $Preloader = $('#preloader', $Container);
	    
	    /* VIDEO CONTROLS */
	    var $Controls = options.controls;
	    if ( $Controls == null ){
		$Controls = $('<div class="videobg-controls"><a href="#mute"></a><a href="#play"></a><a href="#pause"></a><a href="#prev"></a><a href="#next"></a></div>').appendTo($Container);
	    }
	    var $Play = $('[href=#play]', $Controls).hide(),
		$Pause = $('[href=#pause]', $Controls).hide(),
		$Mute = $('[href=#mute]', $Controls).hide(),
		$Prev = $('[href=#prev]', $Controls).hide(),
		$Next = $('[href=#next]', $Controls).hide(),
		$Repeat = $('[href=#repeat]', $Controls),
		$Default = $('.default', $Controls),
		$Additional = $('.additional', $Controls).hide(),
		$EnabledNavigation = true;
	    $Play.on('click', function(){
		PlayVideo();
		$Play.fadeOut(150, function(){
		    $Pause.fadeIn(150);
		    $Mute.show();
		    $Prev.show();
		    $Next.show();
		});
		$Options.onPauseChange.call(this, VideoStatus().paused);
		return false;
	    });
	    $Pause.on('click', function(){
		PauseVideo();
		$Pause.fadeOut(150, function(){
		    $Play.fadeIn(150);
		});
		$Options.onPauseChange.call(this, VideoStatus().paused);
		return false;
	    });
	    $Mute.on('click', function(){
		MuteVideo();
		$Mute.toggleClass('active');
		$Options.onMuteChange.call(this, VideoStatus().muted);
		return false;
	    });
	    $Prev.on('click', function(){
		if ( $EnabledNavigation ){
		    $EnabledNavigation = false;
		    PlayPrevVideo();
		    setTimeout(function(){
			$EnabledNavigation = true;
		    }, 1000);
		}
		return false;
	    });
	    $Next.on('click', function(){
		if ( $EnabledNavigation ){
		    $EnabledNavigation = false;
		    PlayNextVideo();
		    setTimeout(function(){
			$EnabledNavigation = true;
		    }, 1000);
		}
		return false;
	    });
	    $Repeat.on('click', function(){
		PlayVideo(0);
		$Play.hide();
		$Default.show();
		$Additional.hide();
		$Options.onRepeat.call(this);
		return false;
	    });
	    /* /VIDEO CONTROLS */


	    function PlayNextVideo(autoplay){
		PlayVideo('next', autoplay);
	    }
	    function PlayPrevVideo(autoplay){
		PlayVideo('prev', autoplay);
	    }
	    function PlayVideo( position, autoplay ){
		var videoCurr = $Storage.curr().video;
		var videoNew = null;
		if ( position == 'prev' ){
		    if ( !$Options.loop && $Storage.curr().index == $Storage.length - 1) return false;
		    videoNew = $Storage.prev().video;
		}else if ( position == 'next' ){
		    if ( !$Options.loop && $Storage.curr().index == $Storage.length - 1) return false;
		    videoNew = $Storage.next().video;
		}else if ( typeof(parseInt(position)) == 'number' ){
		    videoNew = $Storage.curr(parseInt(position)).video;
		}else{
		    videoNew = videoCurr;
		}
		if ( videoNew == videoCurr ){
		    videoNew.play();
		    if ( $Changed ){
			HandleChange();
		    }
		    $Options.onRepeatPlay.call(this);
		}else{
		    $Changed = true;
		    if ( autoplay || !videoCurr.paused ){
                if ( /safari/.test(navigator.userAgent.toLowerCase()) && !/chrome/.test(navigator.userAgent.toLowerCase()) ){
                    setTimeout(function(){
                        videoNew.play();
                        HandleChange();
                    }, 200);
                }else{
                    videoNew.play();
                    HandleChange();
                }
		    }

		    $(videoCurr).hide();
		    $(videoNew).show();
		    videoCurr.pause();
		    videoCurr.currentTime = 0;
		    $Options.onVideoChange.call(this, $Storage.curr().label, $Storage.curr().keySet, $Storage.curr().keyVideo);
		}
	    }
	    function PauseVideo(){
		$Storage.curr().video.pause();
	    }
	    function MuteVideo(){
		$Storage.curr().video.muted = 1 - $Storage.curr().video.muted;
	    }
	    function VideoStatus(){
		return {
		    paused : $Storage.curr().video.paused,
		    muted : $Storage.curr().video.muted
		}
	    }
	    function HandleChange(){
		if ( $Storage.curr().index == ($Storage.length - 1) && $Options.loopLast ){
		    $Default.fadeOut();
		    $Additional.fadeIn();
		}
		$Options.onChangePlay.call(this, $Storage.curr().label, $Storage.curr().keySet, $Storage.curr().keyVideo);
		$Changed = false;
	    }
	});
    };
})( jQuery );