$(document).ready(function(){
    if ( $('html').hasClass('video') && !/iphone/.test(navigator.userAgent.toLowerCase()) && !/ipad/.test(navigator.userAgent.toLowerCase()) ){
	$('#novideo').remove();
	var audio = null;
	if ( $('html').hasClass('audio') ){
	    audio = document.getElementById('audio');
	}
	var showText = false,
	    textBlock = $('#text'),
	    textBlockItem = null,
	    idleTimer = null,
	    idleCounter = 0,
	    hideInterface = false,
	    blockInterface = false,
	    yaTimer = null,
	    repeat = false;
	var videobg = $('#videobg').videobg({
	    loopLast : true,
	    controls : $('#controls'),
	    videosets : videosets,
	    onVideoChange : function(label, videoset, video){
		$('#videoset .active').removeClass('active');
		var Videoset = $('[data-videoset=' + videoset + ']', $('#videoset'));
		var Video = $('[data-video=' + video + ']', Videoset);
		Video.addClass('active');
		if ( video == 7 ){
		    if ( videoset == 1 ){
			blockInterface = true;
			$('.interface').fadeIn(function(){
			    hideInterface = false;
			});
			resetTimer();
		    }
		    else blockInterface = false;
		    $('img', textBlock).hide();
		    textBlockItem = $('.text-' + videoset, textBlock);
		    textBlockItem.show();
		    showText = true;
		}else{
		    textBlock.css("opacity", 0)
		    showText = false;
		    blockInterface = false;
		}
		repeat = false;
	    },
	    onChangePlay : function(label, videoset, video){
		reachGoal(label);
	    },
	    onPauseChange : function(paused){
		if ( audio ){
		    if ( paused ) audio.pause();
		    else audio.play();
		}
	    },
	    onMuteChange : function(muted){
		if ( audio ) audio.muted = muted;
	    },
	    onPlaybackChange : function(currentTime, duration){
		if ( showText && !repeat ){
		    textBlock.css("opacity", .9 * currentTime / duration);
		}
	    },
	    onRepeatPlay : function(){
		//console.log('repeat');
		repeat = true;
	    },
	    onRepeat : function(){
		reachGoal('PLAY_AGAIN');
	    },
	    onInit : function(){
		$('#videoset ul').fadeIn();
	    }
	});
	$('#videoset a').on('click touchstart', function(){
	    var video = $(this).parents('li');
	    var videoset = video.parents('ul');
	    videobg.trigger({
		type : 'set',
		videoset : videoset.data('videoset'),
		video : video.data('video')
	    });
	    return false;
	});
	$('#controls [href=#play]').on('click touchstart', function(){
	    $('.interface').fadeOut(150, function(){
		hideInterface = true;
	    });
	    if ( audio ) audio.play();
	});
	$('#controls [href=#pause]').on('click touchstart', function(){
	    clearInterval(idleTimer);
	});
	var posX,posY;
	$('body,.interface').on('mousemove touchstart', function(event){
	    if ( /webkit|msie/.test(navigator.userAgent.toLowerCase()) && event.type == 'mousemove' ){
		if ( event.pageX == posX && event.pageY == posY ){
		    return false;
		}else{
		    posX = event.pageX;
		    posY = event.pageY;
		}
	    }
	    idleCounter = 0;
	    if ( hideInterface ){
		hideInterface = false;
		clearInterval(idleTimer);
		idleTimer = setInterval(function(){
		    if ( !blockInterface ) idleCounter += .5;
		    else idleCounter = 0;
		    if ( idleCounter >= 1.5 ) {
			$('.interface').fadeOut(150, function(){
			    hideInterface = true;
			});
		    }
		}, 500);
		$('.interface').fadeIn(150);
	    }
	});
    }else{
	$('#novideo').show();
	$('#videobg, #videoset').remove();
    }
    
    $('a.outbound').on('click', function(){
	var goal = $(this).data('goal'),
	    href = $(this).attr('href');
	reachGoal(goal, href);
	return false;
    });
    
    function resetTimer(){
	idleCounter = 0;
    }
});

function reachGoal(goal, href){
    if ( typeof(yaCounter23269036) != 'undefined' ){
	yaCounter23269036.reachGoal(goal);
	if ( href ){
	    setTimeout(function(){
		location.href = href;
	    }, 150);
	}
    }else{
	setTimeout('reachGoal(goal,href)', 150);
    }
}